package lesson3.Map;

import lesson3.LinkedList.MyLinkedList;

public class MyMap<K, V> {
    private MyLinkedList<Node<K, V>> nodes = new MyLinkedList<>();
    private int size = 0;


    public void put(K key, V value) {
        Node<K, V> newNode;
        if (get(key) == null) {
            newNode = new Node<>(key, value);
            nodes.add(newNode);
            size++;
        } else {
            newNode = getNode(key);
            newNode.value = value;
        }

    }

    private Node getNode(K key) {
        Node<K, V> newNode;
        for (int i = 0; i < size; i++) {
            newNode = nodes.get(i);
            if (newNode.getKey() == key) {
                return newNode;
            }
        }
        return null;
    }


    public V get(K key) {
        Node<K, V> newNode = getNode(key);
        if (newNode != null) {
            return newNode.getValue();
        } else {
            return null;
        }
    }

    public void remove(K key) {
        Node<K, V> newNode;
        for (int i = 0; i < size; i++) {
            newNode = nodes.get(i);
            if (newNode.getKey() == key) {
                nodes.remove(i);
                size--;
                return;
            }
        }
    }

    public MyLinkedList<K> getKeys() {
        MyLinkedList<K> keys = new MyLinkedList<>();
        for (int i = 0; i < size; i++) {
            keys.add(nodes.get(i).getKey());
        }
        return keys;
    }

    public int size() {
        return size;
    }

    private class Node<K, V> {
        private K key;
        private V value;

        private Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public V getValue() {
            return value;
        }

        public K getKey() {
            return key;
        }
    }
}
