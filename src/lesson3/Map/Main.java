package lesson3.Map;

public class Main {

    public static void main(String[] args) {
        MyMap<String, Integer> map = new MyMap<>();
        map.put("h", 15);
        map.put("h", 16);
        map.put("g", 11);
        System.out.println(map.get("h"));
        map.remove("h");
        System.out.println(map.get("h"));
        System.out.println(map.get("g"));
    }
}
