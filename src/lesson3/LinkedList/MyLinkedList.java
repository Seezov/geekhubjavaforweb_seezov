package lesson3.LinkedList;


public class MyLinkedList<T> {
    private Node<T> header;
    private int size = 0;
    private int iterator = -1;

    public MyLinkedList() {
        header = new Node<>(null);
    }

    public int size(){
        return size;
    }

    public T next() {
        if (iterator >= size) {
            return null;
        }
        return get(iterator+=1);
    }

    public T prev() {
        if (iterator <= 0) {
            return null;
        }
        return get(iterator-=1);
    }

    public void add(T value){
        Node<T> newNode = header;
        if(newNode.value == null){
            newNode.value = value;
        }
        else {
            while(newNode.nextNode != null) {
                newNode = newNode.nextNode;
            }
            newNode.nextNode = new Node<>(value);
        }
        size++;
    }

    public void put(T value, int index){
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Enter a correct index, this list has " + size + " elements");
        }
        Node<T> newNode = header;
        for (int i = 0; i < index-1; i++) {
            newNode = newNode.nextNode;
        }
        Node<T> toInsertNode = new Node<>(value);
        toInsertNode.nextNode = newNode.nextNode;
        newNode.nextNode = toInsertNode;
        size++;
    }

    public T get(int index)
    {
        if (index < 0 || index >= size) {
            return null;
        }
        Node<T> newNode = header;
        for (int i = 0; i < index; i++) {
            newNode = newNode.nextNode;
        }
        return newNode.value;
    }

    public void remove(int index){
        Node<T> newNode;
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Enter a correct index, this list has " + size + " elements");
        }
        if(index == 0){
            header = header.nextNode;
        }
        else {
            newNode = header;
            for (int i = 0; i < index - 1; i++) {
                newNode = newNode.nextNode;
            }
            newNode.nextNode = newNode.nextNode.nextNode;
        }
        size--;
    }

    private class Node<T>{
        private T value;
        private Node<T> nextNode;

        private Node(T value) {
            this.value = value;
        }
    }
}
