package lesson3.StringsTest;

public class Main {

    private static long stringTest(){
        long timeBeforeTest = System.currentTimeMillis();
        String currentString = "";
        for (int i = 0; i < 10000; i++) {
            currentString += RandomString.getRandomString(10);
        }
        long timeAfterTest = System.currentTimeMillis();
        return timeAfterTest-timeBeforeTest;
    }

    private static long stringBuilderTest(){
        long timeBeforeTest = System.currentTimeMillis();
        StringBuilder currentString = new StringBuilder();
        for (int i = 0; i < 10000; i++) {
            currentString.append(RandomString.getRandomString(10));
        }
        long timeAfterTest = System.currentTimeMillis();
        return timeAfterTest-timeBeforeTest;
    }

    private static long stringBufferTest(){
        long timeBeforeTest = System.currentTimeMillis();
        StringBuffer currentString = new StringBuffer();
        for (int i = 0; i < 10000; i++) {
            currentString.append(RandomString.getRandomString(10));
        }
        long timeAfterTest = System.currentTimeMillis();
        return timeAfterTest-timeBeforeTest;

    }

    private static void displayGraph(long firstValue, long secondValue, long thirdValue){
        System.out.println("Seconds");
        System.out.println('^');
        for (int i = 0; i < 10; i++) {
            if (i == 0) {
                System.out.println('|' + "  " + firstValue);
            }
            else if (i == 9){
                System.out.println('|' + "    |          " +  secondValue + "             " + thirdValue);
                System.out.println('|' + "    |" + "          |" + "             |");
            }
            else {
                System.out.println('|' + "    |");
            }
        }
        System.out.println("----------------------------------> Type");
        System.out.print(" String  StringBuilder  StringBuffer");
    }

    public static void main(String[] args) {

        long stringTest = stringTest();
        long stringBuilderTest = stringBuilderTest();
        long stringBufferTest = stringBufferTest();

        displayGraph(stringTest, stringBuilderTest, stringBufferTest);
    }


}
