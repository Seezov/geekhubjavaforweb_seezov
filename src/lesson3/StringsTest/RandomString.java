package lesson3.StringsTest;

import java.util.Random;

class RandomString {

    static String getRandomString(int stringLength) {
        String availableCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder randomString = new StringBuilder();
        Random random = new Random();
        while (randomString.length() < stringLength) { // length of the random string.
            int index = (int) (random.nextFloat() * availableCharacters.length());
            randomString.append(availableCharacters.charAt(index));
        }
        return randomString.toString();
    }
}
