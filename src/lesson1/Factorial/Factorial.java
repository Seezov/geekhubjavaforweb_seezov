package lesson1.Factorial;

import java.util.Objects;
import java.util.Scanner;

public class Factorial {

    private static long getFactorial(long n){
        if(n == 0 || n == 1) {
            return 1;
        }
        if(n == 2) {
            return 2;
        }
        return n * getFactorial(n-1);
    }


    public static void main(String[] args) {
        System.out.println("Enter n: ");
        int n;
        Scanner input = new Scanner(System.in);
        try {
            n = input.nextInt();
        }
        catch (Exception exp) {
            System.out.println("Wrong input!");
            return;
        }
        if (n < 0) {
            System.out.println("Wrong input!");
            return;
        }
        String response = "Y";
        if (n > 10) {
            System.out.println("You have entred a large number operation can take some time. Want to continue? (Y/N)");
            input = new Scanner(System.in);
            response = input.nextLine();
        }
        if (Objects.equals(response, "Y")) {
            for (int i = 0; i < n; i++) {
                System.out.print(getFactorial(i) + " ");
            }
        }
    }
}
