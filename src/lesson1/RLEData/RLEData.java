package lesson1.RLEData;

import java.util.Scanner;

public class RLEData {

    public static void main(String[] args) {
        System.out.println("Enter a string: ");
        Scanner input = new Scanner(System.in);
        String string = input.nextLine();
        int count = 0;
        char currentChar;
        char nextChar;
        for (int i = 0; i < string.length(); i++) {
            currentChar = string.charAt(i);
            try {
                nextChar = string.charAt(i + 1);
            } catch (Exception exp) {
                nextChar = ' ';
            }
            if (currentChar == nextChar)
                count++;
            else {
                count++;
                System.out.print(count);
                count = 0;
                System.out.print(currentChar);
            }
        }
    }
}
