package lesson1.GameOfLife;

import java.util.Objects;
import java.util.Scanner;

/**
 * Created by Seezov on 20-Oct-17.
 */
public class GameOfLife {

    private static void setStartingFugure(byte[][] currentArray){
        // Glider
        currentArray[7][15] = 1;
        currentArray[8][16] = 1;
        currentArray[9][14] = 1;
        currentArray[9][15] = 1;
        currentArray[9][16] = 1;

    }

    private static void printArray(byte[][] currentArray) {
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 50; j++) {
                if(currentArray[i][j] == 0) {
                    System.out.print(" ");
                }
                else {
                    System.out.print(currentArray[i][j]);
                }
            }
            System.out.print("\n");
        }
    }

    private static int getNumerOfNeighbors(byte[][] initialArray, int i, int j) {
        int numberOfNeighbors = 0;
        numberOfNeighbors = initialArray[i-1][j-1]+initialArray[i-1][j]+initialArray[i-1][j+1]+
                +initialArray[i][j-1]+initialArray[i][j+1]+initialArray[i+1][j-1]+
                +initialArray[i+1][j]+initialArray[i+1][j+1];
        return numberOfNeighbors;
    }


    private static void iteration(byte[][] initialArray, byte[][] newArray) {
        for (int i = 1; i < 19; i++) {
            for (int j = 1; j < 49; j++) {
                int numberOfNeighbors = getNumerOfNeighbors(initialArray,i,j);
                if(initialArray[i][j] == 1) {
                    if (numberOfNeighbors == 0 || numberOfNeighbors == 1 || numberOfNeighbors >= 4)
                        newArray[i][j] = 0;
                    else
                        newArray[i][j] = 1;
                }
                else
                if (numberOfNeighbors == 3)
                    newArray[i][j] = 1;
            }
        }
    }

    public static void main(String[] args) {

        // Creating two arrays
        byte[][] firstArray = new byte[20][50];
        byte[][] secondArray = new byte[20][50];

        // Printing initial array
        setStartingFugure(firstArray);
        printArray(firstArray);
        // ENTER PRESS
        Scanner input = new Scanner(System.in);
        System.out.print("Press ENTER to start\n");
        String string = input.nextLine();

        while(!Objects.equals(string, "exit")) {
            iteration(firstArray, secondArray);
            printArray(secondArray);
            // Next 2 lines can't be separated into method
            firstArray = secondArray;
            secondArray = new byte[20][50];
            System.out.print("Type 'exit' to exit\n");
            string = input.nextLine();
        }
    }
}
