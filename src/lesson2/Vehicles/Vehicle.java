package lesson2.Vehicles;

class Vehicle implements Driveable {

    private int amountOfFuel;
    private int engineCapacity;

    public Vehicle(int amountOfFuel, int engineCapacity){
        this.amountOfFuel = amountOfFuel;
        this.engineCapacity = engineCapacity;
    }

    @Override
    public void accelerate() {
        amountOfFuel--;
        engineCapacity++;
        System.out.println("Engine is accelerating!");
    }

    @Override
    public void brake() {
        engineCapacity--;
        if(engineCapacity > 0) {
            System.out.println("Engine capacity is " + engineCapacity);
        }
        else {
            System.out.println("Engine stopped");
            engineCapacity = 0;
        }
    }

    @Override
    public void turn() {
        System.out.println("Wheels are turning");
    }

    public void refill(){
        amountOfFuel = 10;
        System.out.println("Vehicle is refilled");
    }
}
