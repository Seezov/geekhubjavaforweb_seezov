package lesson2.Vehicles;

class Boat extends Vehicle {

    public Boat(int amountOfFuel, int engineCapacity) {
        super(amountOfFuel, engineCapacity);
    }

    @Override
    public void turn() {
        System.out.println("Boat is turning");
    }
}
