package lesson2.Vehicles;

public interface Driveable {
    void accelerate();
    void brake();
    void turn();
}