package lesson2.Vehicles;

public class Main {

    public static void main(String[] args) {
        SolarCar solarCar = new SolarCar(10,1);
        solarCar.accelerate();
        solarCar.turn();
        solarCar.brake();
        solarCar.brake();
    }
}
