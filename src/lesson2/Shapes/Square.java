package lesson2.Shapes;

public class Square extends Shape {

    private double side;

    public Square(double side) {
        this.side = side;
    }

    @Override
    public double calculateArea() {
        area = side  * side;
        return area;
    }

    @Override
    public double calculatePerimeter() {
        perimeter = side * 4;
        return perimeter;
    }
}
