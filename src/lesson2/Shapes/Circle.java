package lesson2.Shapes;

public class Circle extends Shape {

    private double radius;

    public Circle(double radius){
        this.radius = radius;
    }

    @Override
    public double calculateArea() {
        area = Math.PI * Math.pow(radius,2.0);
        return area;
    }

    @Override
    public double calculatePerimeter() {
        perimeter = 2 * Math.PI * radius;
        return perimeter;
    }


}
