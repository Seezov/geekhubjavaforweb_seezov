package lesson2.Shapes;

abstract class Shape {

    double area;
    double perimeter;

    public abstract double calculateArea();
    public abstract double calculatePerimeter();
}
