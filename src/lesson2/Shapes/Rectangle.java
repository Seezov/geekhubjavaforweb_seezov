package lesson2.Shapes;

public class Rectangle extends Shape {

    private double height;
    private double width;

    public Rectangle(double height, double width) {
        this.height = height;
        this.width = width;
    }

    @Override
    public double calculateArea() {
        area = height * width;
        return area;
    }

    @Override
    public double calculatePerimeter() {
        perimeter = (height + width) * 2;
        return perimeter;
    }
}
