package lesson2.Shapes;

import java.util.Scanner;

public class Main {

    public enum Shapes{
        CIRCLE,TRIANGLE,SQUARE,RECTANGLE
    }

    private static void userChooseCircle(){
        System.out.print("Enter a length of radius:\n");
        Scanner input = new Scanner(System.in);
        double radius = input.nextDouble();
        Circle circle = new Circle(radius);
        System.out.print("The area of the circle is: " + circle.calculateArea());
        System.out.print("\nThe perimeter of the circle is: " + circle.calculatePerimeter());
    }

    private static void userChooseTriangle(){
        System.out.print("Enter length of sides of the triangle:\n");
        Scanner input = new Scanner(System.in);
        double sideA = input.nextDouble();
        double sideB = input.nextDouble();
        double sideC = input.nextDouble();
        Triangle triangle = new Triangle(sideA, sideB, sideC);
        System.out.print("The area of the triangle is: " + triangle.calculateArea());
        System.out.print("\nThe perimeter of the triangle is: " + triangle.calculatePerimeter());
    }

    private static void userChooseSquare(){
        System.out.print("Enter length of side of the square:\n");
        Scanner input = new Scanner(System.in);
        double side = input.nextDouble();
        Square square = new Square(side);
        System.out.print("The area of the square is: " + square.calculateArea());
        System.out.print("\nThe perimeter of the square is: " + square.calculatePerimeter());
        System.out.print("\nSquare can also be represented as 2 equivalent Triangle figures\n");
        double hypotenuse = Math.sqrt(Math.pow(side,2) + Math.pow(side,2));
        Triangle triangle = new Triangle(side, side, hypotenuse);
        System.out.print("The area of these triangles is: " + triangle.calculateArea());
        System.out.print("\nThe perimeter of these triangles is: " + triangle.calculatePerimeter());
    }

    private static void userChooseRectangle(){
        System.out.print("Enter length of  height and width of the rectangle:\n");
        Scanner input = new Scanner(System.in);
        double height = input.nextDouble();
        double width = input.nextDouble();
        Rectangle rectangle = new Rectangle(height,width);
        System.out.print("The area of the rectangle is: " + rectangle.calculateArea());
        System.out.print("\nThe perimeter of the rectangle is: " + rectangle.calculatePerimeter());
        System.out.print("\nRectangle can also be represented as 2 equivalent Triangle figures\n");
        double hypotenuse = Math.sqrt(Math.pow(height,2) + Math.pow(width,2));
        Triangle triangle = new Triangle(height, width, hypotenuse);
        System.out.print("The area of these triangles is: " + triangle.calculateArea());
        System.out.print("\nThe perimeter of these triangles is: " + triangle.calculatePerimeter());
    }

    public static void main(String[] args) {
        System.out.print("Enter a particular shape (CIRCLE, TRIANGLE, SQUARE, RECTANGLE):\n");
        Scanner input = new Scanner(System.in);
        String choosedShape = input.nextLine();
        Shapes currentShape;
        try {
            currentShape = Shapes.valueOf(choosedShape);
        }catch(Exception e){
            System.out.println("There is no \"" + choosedShape + "\" shape");
            return;
        }
        switch (currentShape){
            case CIRCLE:
                userChooseCircle();
                break;
            case TRIANGLE:
                userChooseTriangle();
                break;
            case SQUARE:
                userChooseSquare();
                break;
            case RECTANGLE:
                userChooseRectangle();
                break;
        }
    }
}
